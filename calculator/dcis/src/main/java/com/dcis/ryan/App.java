package com.dcis.ryan;

import java.util.ArrayList;
import java.util.Scanner;

public final class App {
    private App() {
    }

    public static void main(String[] args) {
        ArrayList<Double> numbers = new ArrayList<Double>();
        ArrayList<Character> operators = new ArrayList<Character>();
         
        Scanner scanner = new Scanner(System.in);
        try {
            do {
                System.out.println("\nEnter a number: \n");
                numbers.add(scanner.nextDouble());
                System.out.println("\nEnter an operation sign such as, '+', '-', '*', or '/', '=': \n");
                char operator = scanner.next().charAt(0);
                if (operator == '=')
                    break;
                operators.add(operator);
            } while (true);
        } finally {
            scanner.close();
        }
        
        Double answer = numbers.remove(0);
        String resultText = "" + answer;
        
        for (int i=0; i<operators.size(); ++i) {
            char operator = operators.get(i);
            
            Double number = numbers.get(i);
            
            switch(operator) {

                case '+':
                    answer += number;
                    break;
    
                case '-':
                    answer -= number;
                    break;
    
                case '*':
                    answer *= number;
                    break;
    
                case '/':
                    answer /= number;
                    break;
    
                default:
                    System.out.println("Wrong choice for operator. ");
                    break;
            }
            
            resultText += " " + operator + " " + number;
        }
        
        System.out.println("\n" + resultText + " = " + answer);
    

    }
}





// int num1 = 22, num2 = 33, sum;
// sum = num1 + num2;
// System.out.println("Sum of these two numbers is: "+sum);

// int num1, num2, sum;

// Scanner sc = new Scanner(System.in);
// System.out.println("Enter first number: ");
// num1 = sc.nextInt();

// System.out.println("Enter second number: ");
// num2 = sc.nextInt();

// sc.close();
// sum = num1 + num2;
// System.out.println("The sum of these two numbers is: "+sum);
